@extends('layouts.app')

@section('content')
    <h1>Categories</h1>
    @foreach ($categories as $category)
        <div>
            <h2>{{ $category->name }}</h2>
            @if($category->children)
                <ul>
                    @foreach($category->children as $child)
                        <li>{{ $child->name }}</li>
                    @endforeach
                </ul>
            @endif
        </div>
    @endforeach
@endsection
