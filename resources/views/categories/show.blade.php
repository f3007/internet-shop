@extends('layouts.app')

@section('content')
    <h1>{{ $category->name }}</h1>
    <p>{{ $category->description }}</p>

    @if($category->children)
        <h2>Subcategories:</h2>
        <ul>
            @foreach($category->children as $child)
                <li><a href="{{ route('categories.show', ['category' => $child->id]) }}">{{ $child->name }}</a></li>
            @endforeach
        </ul>
    @endif

    @if($category->products)
        <h2>Products:</h2>
        @foreach ($category->products as $product)
            <div>
                <h3><a href="{{ route('products.show', ['product' => $product->id]) }}">{{ $product->name }}</a></h3>
                <p>{{ $product->description }}</p>
                <p>Price: {{ $product->price }}</p>
            </div>
        @endforeach
    @endif
@endsection
