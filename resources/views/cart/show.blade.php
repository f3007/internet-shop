@extends('layouts.app')

@section('content')
    <h1>Your Cart</h1>
    @foreach ($cart->products as $product)
        <div>
            <h2>{{ $product->name }}</h2>
            <p>Price: {{ $product->price }}</p>
            <a href="{{ route('cart.remove', ['product' => $product->id]) }}">Remove from Cart</a>
        </div>
    @endforeach
@endsection
