@extends('layouts.app')

@section('content')
    <h1>{{ $product->name }}</h1>
    <p>{{ $product->description }}</p>
    <p>Price: {{ $product->price }}</p>
    <a href="{{ route('cart.add', ['product' => $product->id]) }}">Add to Cart</a>
@endsection
