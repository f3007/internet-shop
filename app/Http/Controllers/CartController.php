<?php

namespace App\Http\Controllers;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function show(Cart $cart)
    {
        return view('cart.show', compact('cart'));
    }

    public function addProduct(Cart $cart, Product $product)
    {
        $cart->products()->attach($product);
        return redirect()->back();
    }

    public function removeProduct(Cart $cart, Product $product)
    {
        $cart->products()->detach($product);
        return redirect()->back();
    }
}
